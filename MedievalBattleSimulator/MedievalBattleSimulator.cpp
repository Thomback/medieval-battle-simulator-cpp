// Includes
#include <iostream>
#include <string>
#include <list>
#include <cmath>


template<typename trucAAfficher>
void presenter(trucAAfficher nomAAfficher) {
    std::cout << "*Vous jettez un coup d'oeil � ce magnifique " << nomAAfficher << ".*\n";
}

/// <summary>
/// Arme class
/// </summary>
class Arme {
public:
    int degats = 0;
    std::string  nomArme = "";

    // Default constructor
    Arme() {    
        degats = 3;
        nomArme = "Epee";
    }

    // Constructor overloading
    Arme(int degats, std::string nomArme) { 
        this->degats = degats;
        this->nomArme = nomArme;
    }
    
    /// <summary>
    /// Change weapon characteristics : overload 1
    /// </summary>
    /// <param name="newArme">new Arme reference</param>
    void changer(Arme& newArme) {           
        this->degats = newArme.degats;
        this->nomArme = newArme.nomArme;
    }

    /// <summary>
    /// Change weapon characteristics : overload 2
    /// </summary>
    /// <param name="newDegats">new damage</param>
    /// <param name="newNomArme">new name</param>
    void changer(int newDegats, std::string newNomArme) {
        this->degats = newDegats;
        this->nomArme = newNomArme;
    }

    friend void presenter(std::string nomAAfficher);
};

/// <summary>
/// Personnage class
/// </summary>
class Personnage {
protected:
    int vie = 0;
    int mana = 0;

    Arme* myArme;

public:
    std::string nom = "";

    // Default constructor
    Personnage() {
        nom = "Aventurier";
        vie = 15;
        mana = 3;

        myArme = new Arme();
    }

    // Constructor overload 1 : All variables as parameters
    Personnage(std::string nom, int vie, int mana, int degats, std::string nomArme) {
        this->nom = nom;
        this->vie = vie;
        this->mana = mana;

        this->myArme = new Arme(degats, nomArme);
    }

    // Constructor overload 2 : Personnage parameter
    Personnage(Personnage& persoACopier) {
        this->vie = persoACopier.vie;
        this->mana = persoACopier.mana;
        this->nom = persoACopier.nom;

        this->myArme = persoACopier.myArme;
    }

    /// <summary>
    /// Receive damage and checks if character is dead
    /// </summary>
    /// <param name="nombreDegats">number of damage received</param>
    void recevoirDegats(int nombreDegats) {
        this->vie -= nombreDegats;

        if (!estVivant()) {
            std::cout << "|-- Personnage '" + this->nom + "' est mort --|\n";
            this->vie = 0;
        }
    }

    /// <summary>
    /// Attacks another Personnage with current attack power
    /// </summary>
    /// <param name="persoAAttaquer">Reference to the other Personnage</param>
    void attaquer(Personnage& persoAAttaquer) {
        std::cout << this->nom + " attaque " << persoAAttaquer.nom << " pour "<< this->myArme->degats <<
            " points de d�g�ts � l'aide de son " << this->myArme->nomArme << "!\n";
        persoAAttaquer.recevoirDegats(this->myArme->degats);
    }
    
    /// <summary>
    /// Change current weapon's damage and name
    /// </summary>
    /// <param name="degatsArme">new damage</param>
    /// <param name="nomArme">new name</param>
    void changerArme(int degatsArme, std::string nomArme) {
        this->myArme->changer(degatsArme, nomArme);
    }

    /// <summary>
    /// Checks if the character is dead
    /// </summary>
    /// <returns>is he dead?</returns>
    bool estVivant() {
        if (vie <= 0) {
            return false;
        }
        else {
            return true;
        }
    }

    friend void presenter(std::string nomAAfficher);
};

/// <summary>
/// Character child 1/3
/// </summary>
class Guerrier : public Personnage {
public:
    Guerrier() {
        nom = "Guerrier";
        vie = 20;
        mana = 0;

        std::string nomDeLarme = "Hache de guerre";
        myArme = new Arme(4, nomDeLarme);
    }

    Guerrier(std::string nouveauNom) {
        nom = nouveauNom;
        vie = 20;
        mana = 0;

        std::string nomDeLarme = "Hache de guerre";
        myArme = new Arme(4, nomDeLarme);
    }

    /// <summary>
    /// Attacks another Personnage with current attack power
    /// Has a 1/3 chance to boost the attack
    /// </summary>
    /// <param name="persoAAttaquer">Reference to the other Personnage</param>
    void attaquer(Personnage& persoAAttaquer) {
        if (rand() % 3 <= 0) {
            std::cout << this->nom + " attaque avec boost pour " << this->myArme->degats + 1 <<
                " points de d�g�ts avec sa" << this->myArme->nomArme << "!\n";
            persoAAttaquer.recevoirDegats(this->myArme->degats + 1);
        }
        else {
            std::cout << this->nom + " attaque pour " << this->myArme->degats <<
                " points de d�g�ts � l'aide de sa " << this->myArme->nomArme << "!\n";
            persoAAttaquer.recevoirDegats(this->myArme->degats);
        }
    }
};


/// <summary>
/// Character child 2/3
/// </summary>
class Mage : public Personnage {
public:
    Mage() {
        nom = "Mage";
        vie = 13;
        mana = 10;

        std::string nomDeLarme = "Baton magique";
        myArme = new Arme(2, nomDeLarme);
    }

    Mage(std::string nouveauNom) {
        nom = nouveauNom;
        vie = 13;
        mana = 10;

        std::string nomDeLarme = "Baton magique";
        myArme = new Arme(2, nomDeLarme);
    }

    /// <summary>
    /// Heals an ally but costs 2 mana point
    /// </summary>
    /// <param name="persoASoigner">Personnage to heal</param>
    /// <returns>has the spell succeded?</returns>
    bool soigner(Personnage& persoASoigner) {
        if (this->mana >= 2) {
            std::cout << this->nom + " soigne son ali� pour " << this->myArme->degats * 2 <<
                " points de vie � l'aide de son " << this->myArme->nomArme << "!\n";
            persoASoigner.recevoirDegats(this->myArme->degats * -2);
            this->mana -= 2;
            return true;
        }
        else {
            std::cout << this->nom + " essaie de soigner son ali� mais n'a plus de mana.\n";
            return false; // Sort fail
        }
    }
};

/// <summary>
/// Character child 3/3
/// </summary>
class Archer : public Personnage {
public:
    Archer() {
        nom = "Archer";
        vie = 17;
        mana = 3;

        std::string nomDeLarme = "Baton magique";
        myArme = new Arme(2, nomDeLarme);
    }

    Archer(std::string nouveauNom) {
        nom = nouveauNom;
        vie = 17;
        mana = 3;

        std::string nomDeLarme = "Baton magique";
        myArme = new Arme(2, nomDeLarme);
    }

    /// <summary>
    /// Has a 1/3 chance to dodge the attack
    /// </summary>
    /// <param name="nombreDegats">number of damage to receive</param>
    void recevoirDegats(int nombreDegats) {
        if (rand() % 3 > 0) {
            this->vie -= nombreDegats;
            if (!estVivant()) {
                std::cout << "|-- Personnage '" + this->nom + "' est mort --|\n";
                this->vie = 0;
            }
        }
        else {
            std::cout << this->nom + "' a esquive l'attaque!\n";
        }

    }
};

std::string userTextInput(std::string textToDisplay, std::list<std::string> possibleResponses) {
    std::string response = "";
    bool validResponse = false;

    while (!validResponse) {
        std::cout << textToDisplay <<"\n";
        std::cin >> response;

        if (std::find(possibleResponses.begin(), possibleResponses.end(), response) != possibleResponses.end()) {
            validResponse = true;
        }
        else {
            std::cout << "~ Reponse incorrecte, respectez la casse. ~\n";
        }
    }
    return response;
};

struct team {
    Guerrier* guerrier;
    Mage* mage;
    Archer* archer;
};

void presentationEquipe(team laTeam) {
    std::cout << "\nGUERRIER : " << laTeam.guerrier->nom << " (lvl " << (rand() % 5) + 1 <<
        ")\nMAGE : " << laTeam.mage->nom << " (lvl " << (rand() % 5) + 1 <<
        ")\nARCHER : " << laTeam.archer->nom << " (lvl " << (rand() % 5) + 1 << ")\n";
}

void tourJoueur(team teamJoueur, team teamEnnemi) {
    std::string selectedChara;
    std::string selectedEnemy;
    std::cout << "\nAvec quel personnage souhaitez-vous agir?\n";
    selectedChara = userTextInput(teamJoueur.guerrier->nom + ", " + teamJoueur.mage->nom + " ou "+teamJoueur.archer->nom+"?\n",
        { teamJoueur.guerrier->nom,teamJoueur.mage->nom,teamJoueur.archer->nom });

    std::cout << selectedChara <<" agit, qui voulez-vous attaquer?\n";
    selectedEnemy = userTextInput(teamEnnemi.guerrier->nom + ", " + teamEnnemi.mage->nom + "ou " + teamEnnemi.archer->nom + "?\n",
        { teamEnnemi.guerrier->nom,teamEnnemi.mage->nom,teamEnnemi.archer->nom });

    std::cout << selectedChara << " attaque "<< selectedEnemy << "!\n";

}

void tourEnemi(team teamJoueur, team teamEnnemi) {
    std::string enemyToAct;
    switch (rand()%3)
    {
        case 0:
            enemyToAct = teamEnnemi.guerrier->nom;
            break;
        case 1:
            enemyToAct = teamEnnemi.mage->nom;
            break;
        case 2:
            enemyToAct = teamEnnemi.archer->nom;
            break;
        default:
            enemyToAct = teamEnnemi.guerrier->nom;
            break;
    }
    std::cout << "\nVous etes trop rapide, "<<enemyToAct<<" n'a pas eu le temps d'agir!\n";
}

void battleManager() {
    

    team playerTeam;
    team ordiTeam;

    std::string persoName;

    std::cout << "Entrez un nom pour votre Guerrier :\n";
    std::cin >> persoName;
    playerTeam.guerrier = new Guerrier(persoName);

    std::cout << "Entrez un nom pour votre Mage :\n";
    std::cin >> persoName;
    playerTeam.mage = new Mage(persoName);

    std::cout << "Entrez un nom pour votre Archer :\n";
    std::cin >> persoName;
    playerTeam.archer = new Archer(persoName);

    ordiTeam.guerrier = new Guerrier("Pontius");
    ordiTeam.mage = new Mage("Amadeus");
    ordiTeam.archer = new Archer("Zoya");


    // Combat start

    std::cout << "\nLe combat opposera l'equipe du joueur : \n";
    presentationEquipe(playerTeam);
    std::cout << "\nContre l'equipe du developpeur : \n";
    presentationEquipe(ordiTeam);

    int playerInit = (rand() % 6) + 1;
    int ordiInit = (rand() % 6) + 1;

    bool flipFlop = true;

    std::cout << "\nVous avez fait "<< playerInit << " a votre initiative alors que le developpeur a fait "<< ordiInit << ".\n";
    if (playerInit >= ordiInit) {
        std::cout << "Vous jouez en premier.\n";
    }
    else {
        std::cout << "Le developpeur joue en premier.\n";
        flipFlop = !flipFlop;
    }
    int compteurTours = 0;

    while (
        (playerTeam.guerrier->estVivant() || playerTeam.mage->estVivant() || playerTeam.archer->estVivant()) &&
        (ordiTeam.guerrier->estVivant() || ordiTeam.mage->estVivant() || ordiTeam.archer->estVivant())
        ) {

        if (flipFlop) {
            tourJoueur(playerTeam,ordiTeam);
            if (playerInit < ordiInit) {
                compteurTours++;
            }
        }
        else {
            tourEnemi(playerTeam, ordiTeam);
            if (ordiInit <= playerInit) {
                compteurTours++;
            }
        }
    }


    if (playerTeam.guerrier->estVivant() || playerTeam.mage->estVivant() || playerTeam.archer->estVivant()) {
        std::cout << "\n\n ~ Felicitations, vous avez remporte le combat en "<< compteurTours << " tours! ~ \n";
    }
    else {
        std::cout << "\n\n ~ Dommage, vous avez perdu le combat en " << compteurTours << " tours... ~ \n";
    }
}

// -- Main Function --
int main()
{
    battleManager();
}


